package nl.app.com.jtest

import androidx.test.ext.junit.runners.AndroidJUnit4
import dagger.Component
import nl.app.com.jtest.core.di.module.ImageBindModule
import nl.app.com.jtest.core.di.module.NetworkModule
import nl.app.com.jtest.data.api.ApiService
import nl.app.com.jtest.data.model.PaginationParams
import nl.app.com.jtest.data.repository.ImageRepository
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import javax.inject.Inject
import javax.inject.Singleton
import com.facebook.soloader.SoLoader
import org.junit.BeforeClass


//testing just the availability and parsing
@RunWith(AndroidJUnit4::class)
class SimpleApiTest {

    companion object {
        @BeforeClass
        @JvmStatic
        fun setup() {
            SoLoader.setInTestMode()
        }
    }

    @Inject
    lateinit var apiService : ApiService

    @Singleton
    @Component(modules = [NetworkModule::class, ImageBindModule::class])
    interface TestComponent {
        fun inject(t: SimpleApiTest)
    }

    @Before
    fun before() {
        val testComponent = DaggerSimpleApiTest_TestComponent
                .builder()
                .networkModule(NetworkModule())
                .build()
        testComponent.inject(this)
    }

    @Test
    fun testImage() {
        apiService.getImage("uj9jR91ftsPqEF5ETg")
                .doOnError { println("got error $it") }
                .doOnSuccess { println("got result = $it") }
                .test()
                .assertComplete()
    }

    @Test
    fun testTrending() {
        val offset = 25
        apiService.getTrending(0, offset)
                .doOnError { println("got error $it") }
                .doOnSuccess { println("got result = $it") }
                .test()
                .assertComplete()
                .assertValue { it.pagination.offset == offset }
    }

    //api service

    @Inject
    lateinit var rep: ImageRepository

    @Test
    fun testRepository_firstPage() {
        rep.getTrending(PaginationParams(0, 25))
                .doOnError { println("got error $it") }
                .doOnSuccess { println("got result = $it") }
                .test()
                .assertComplete()
    }

    @Test
    fun testRepository_image() {
        rep.getImage("13zmwxqp1V4PxxFGqr")
                .doOnError { println("got error $it") }
                .doOnSuccess { println("got result = $it") }
                .test()
                .assertComplete()
    }

}