package nl.app.com.jtest;

import com.squareup.rx2.idler.Rx2Idler;

import androidx.test.runner.AndroidJUnitRunner;
import io.reactivex.plugins.RxJavaPlugins;

public class TestRunner extends AndroidJUnitRunner {
    @Override
    public void onStart() {
        RxJavaPlugins.setInitComputationSchedulerHandler(
                Rx2Idler.create("RxJava 2.x Computation Scheduler"));
        RxJavaPlugins.setInitIoSchedulerHandler(
                Rx2Idler.create("RxJava 2.x IO Scheduler"));
        super.onStart();
    }
}
