package nl.app.com.jtest

import androidx.navigation.findNavController
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions.scrollToPosition
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.rule.ActivityTestRule
import nl.app.com.jtest.domain.pagination.PaginationConfig
import nl.app.com.jtest.presentation.activity.ImageActivity
import nl.app.com.jtest.presentation.fragment.ImageDetailFragmentArgs
import nl.app.com.jtest.presentation.viewholder.ImagesViewHolder
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class ImageActivityTest {
    @get:Rule
    val activityRule = ActivityTestRule(ImageActivity::class.java)

    @Test
    fun headerIsDisplayed() {
        onView(withText("JTest")).check(matches(isDisplayed()))
    }

    @Test
    fun testList() {
        val lastPosition = PaginationConfig().getPageSize() - 1

        onView(withId(R.id.rv)).check(matches(isDisplayed()))
        onView(RecyclerViewMatcher(R.id.rv).atPosition(3))
                .check(matches(withId(R.id.list_image)))

        onView(withId(R.id.rv)).perform(scrollToPosition<ImagesViewHolder>(lastPosition))
        onView(RecyclerViewMatcher(R.id.rv).atPosition(lastPosition))
                .check(matches(withId(R.id.list_image)))
    }

    @Test
    fun testDetailNavigate() {
        activityRule.activity.apply {
            runOnUiThread {
                //actually would give an error toast, but we're checking just visibility to ensure
                //that we're opened needed screen
                val bundle = ImageDetailFragmentArgs.Builder("testGifId").build().toBundle()
                findNavController(R.id.nav_host_fragment).navigate(R.id.imageDetailFragment, bundle)
            }
        }
        onView(withId(R.id.image)).check(matches(isDisplayed()))
    }

}