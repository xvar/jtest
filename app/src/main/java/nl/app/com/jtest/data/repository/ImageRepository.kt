package nl.app.com.jtest.data.repository

import io.reactivex.Flowable
import io.reactivex.Maybe
import io.reactivex.Single
import nl.app.com.jtest.data.model.ImageModel
import nl.app.com.jtest.data.model.PaginationParams


interface ImageRepository {
    fun getTrending(params: PaginationParams) : Single<List<ImageModel>>
    fun getImage(id: String) : Maybe<ImageModel>
    fun getAll() : Flowable<List<ImageModel>>
    fun clear()
}