package nl.app.com.jtest.presentation.ktx

import androidx.annotation.StringRes
import nl.app.com.jtest.R
import nl.app.com.jtest.data.model.throwable.UrlNotSupportedException
import java.net.SocketTimeoutException
import java.net.UnknownHostException
import java.util.concurrent.TimeoutException

enum class ErrorType {
    NETWORK, TIMEOUT, URL_NOT_SUPPORTED, UNKNOWN
}

fun <T> Throwable.returnOnType(function: (ErrorType, Throwable) -> T) : T {
    return when(this) {
        is UnknownHostException -> function(ErrorType.NETWORK, this)
        is SocketTimeoutException, is TimeoutException -> function(ErrorType.TIMEOUT, this)
        is UrlNotSupportedException -> function(ErrorType.URL_NOT_SUPPORTED, this)
        else -> function(ErrorType.UNKNOWN, this)
    }
}

//non-production, just for the sake of example
@StringRes
fun Throwable.getDefaultErrorId() : Int {
    return returnOnType { errorType, _ ->
         when (errorType) {
            ErrorType.NETWORK -> R.string.error_connection
            ErrorType.TIMEOUT -> R.string.error_timeout
            ErrorType.UNKNOWN -> R.string.error_unknown
            ErrorType.URL_NOT_SUPPORTED -> R.string.url_not_supported
         }
    }
}
