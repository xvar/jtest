package nl.app.com.jtest.presentation.activity

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.core.view.ViewCompat
import androidx.navigation.findNavController
import androidx.navigation.fragment.FragmentNavigatorExtras
import io.reactivex.functions.Consumer
import nl.app.com.jtest.R
import nl.app.com.jtest.core.di.ViewModelFactory
import nl.app.com.jtest.presentation.fragment.ImageListFragmentDirections
import nl.app.com.jtest.presentation.model.IMAGE_DETAIL_TRANSITION
import nl.app.com.jtest.presentation.model.RouteEvent
import nl.app.com.jtest.presentation.vm.ImageDetailViewModel
import nl.app.com.jtest.presentation.vm.ImageListViewModel
import javax.inject.Inject


class ImageActivity : BaseActivity() {

    @Inject
    lateinit var listFactory: ViewModelFactory<ImageListViewModel>
    @Inject
    lateinit var detailFactory: ViewModelFactory<ImageDetailViewModel>
    private lateinit var listVm: ImageListViewModel
    private lateinit var detailVm: ImageDetailViewModel

    @Inject
    lateinit var router: Router

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_image_list)

        listVm = listFactory.get()
        detailVm = detailFactory.get()

        addDisposable("route", getRouteEvents().subscribe(router))
    }

    private fun getRouteEvents() = listVm.routeEvents().mergeWith(detailVm.routeEvents())

    override fun onSupportNavigateUp(): Boolean
            = findNavController(R.id.nav_host_fragment).navigateUp()

    override fun onBackPressed() {
        if (!findNavController(R.id.nav_host_fragment).popBackStack()) {
            super.onBackPressed()
        }
    }


    class Router(private val a: ImageActivity) : Consumer<RouteEvent> {

        override fun accept(r: RouteEvent) {
            when(r) {
                is RouteEvent.ImageDetail -> {
                    val detailAction = ImageListFragmentDirections.ActionImageDetails(r.imageId)
                    ViewCompat.setTransitionName(r.shared, IMAGE_DETAIL_TRANSITION)
                    val extras = FragmentNavigatorExtras(r.shared to IMAGE_DETAIL_TRANSITION)
                    r.shared.findNavController().navigate(detailAction, extras)
                }
                is RouteEvent.UserProfile -> {
                    //url is being validated in the view model
                    val intent = Intent(Intent.ACTION_VIEW, Uri.parse(r.url))
                    a.startActivity(intent)
                }
            }
        }
    }
}
