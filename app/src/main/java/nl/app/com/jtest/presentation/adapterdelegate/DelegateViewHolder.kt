package nl.app.com.jtest.presentation.adapterdelegate

import android.view.View
import androidx.recyclerview.widget.RecyclerView

abstract class DelegateViewHolder<T, P>(itemView: View) : RecyclerView.ViewHolder(itemView) {

    @Suppress("UNCHECKED_CAST")
    fun bind(item: T, payloads: List<Any>?) {
        if (payloads != null && payloads.isNotEmpty()) {
            bind(payloads as List<P>)
        } else {
            bind(item)
        }
    }

    open fun bind(payloads: List<P>) {}

    abstract fun bind(item: T)
}
