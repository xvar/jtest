package nl.app.com.jtest.core.di.module

import dagger.Binds
import dagger.Module
import nl.app.com.jtest.data.repository.ComposedImageRepositoryImpl
import nl.app.com.jtest.data.repository.ImageRepository
import nl.app.com.jtest.data.repository.ImageRepositoryImpl
import nl.app.com.jtest.domain.interactor.ImageListInteractor
import nl.app.com.jtest.domain.interactor.ImageListPaginationInteractor
import nl.app.com.jtest.presentation.custom.FrescoImageLoader
import nl.app.com.jtest.presentation.custom.ImageLoader

@Module
interface ImageBindModule {
    @Binds
    fun bindListInteractor(i: ImageListInteractor): ImageListPaginationInteractor

    @Binds
    fun bindRepository(r: ImageRepositoryImpl) : ImageRepository //more optimal (just a little)

    @Binds
    fun bindImageLoader(r: FrescoImageLoader): ImageLoader
}