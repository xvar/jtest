package nl.app.com.jtest.domain.interactor

import io.reactivex.Flowable

interface PaginationInteractor<T> {

    fun loadFirst()
    fun loadNext()
    fun reload()
    fun clear()
    fun getPageSize() : Int
    fun stopPagination() : Boolean

    fun observe() : Flowable<State<T>>
}