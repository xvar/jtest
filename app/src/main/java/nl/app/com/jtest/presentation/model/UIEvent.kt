package nl.app.com.jtest.presentation.model

import android.view.View

const val UI_IMAGE_CLICK = -165
const val UI_TOGGLE_SETTINGS = -167
const val UI_RELOAD = -168
const val UI_PROFILE_CLICK = -169

const val IMAGE_DETAIL_TRANSITION = "image_detail_element"

sealed class UIEvent {
    open class Base(val id: Int) : UIEvent()
    class ImageClick(val imageId: String, val view: View): Base(UI_IMAGE_CLICK)
    class ProfileClick(val url: String): Base(UI_PROFILE_CLICK)

    //one can be absolutely fine without next classes
    //Just a little more readable (for the example's sake)
    class ToggleClick: Base(UI_TOGGLE_SETTINGS)
    class ReloadClick: Base(UI_RELOAD)
}
