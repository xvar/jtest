package nl.app.com.jtest.data.model

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty

@JsonIgnoreProperties(ignoreUnknown = true)
data class Pagination(
        @JsonProperty("limit") val offset : Int,
        @JsonProperty("total_count") val totalCount : Int,
        @JsonProperty("count") val count : Int
)