package nl.app.com.jtest.domain.model

import io.reactivex.functions.Function
import nl.app.com.jtest.data.model.ImageModel
import nl.app.com.jtest.data.repository.SettingsRepository
import javax.inject.Inject

//I'll use these models in the presentation layer for the sake of simplicity
//In general case, presentation layer should have its own model

class ImageModelMapper @Inject constructor(
        private val r: SettingsRepository
): Function<ImageModel, DomainModel.ImageListModel> {
    override fun apply(t: ImageModel): DomainModel.ImageListModel {
        val url = if (r.isStaticImage()) t.images.fixedWidthStill.url else t.images.fixedWidth.url
        return DomainModel.ImageListModel(url = url, id = t.id)
    }
}

class ImageListModelMapper @Inject constructor(
        private val mapper: ImageModelMapper
): Function<List<ImageModel>, List<DomainModel.ImageListModel>> {
    override fun apply(t: List<ImageModel>): List<DomainModel.ImageListModel> {
        return t.mapIndexed { _, imageModel -> mapper.apply(imageModel) }
    }

}

class ImageDetailMapper @Inject constructor(
        private val r: SettingsRepository
): Function<ImageModel, DomainModel.ImageDetailModel> {
    override fun apply(t: ImageModel): DomainModel.ImageDetailModel {
        val url = if (r.isStaticImage()) t.images.fixedWidthStill.url else t.images.fixedWidth.url
        return DomainModel.ImageDetailModel(url, t.user)
    }

}