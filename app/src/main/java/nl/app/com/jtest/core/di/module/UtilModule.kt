package nl.app.com.jtest.core.di.module

import android.app.Application
import android.content.SharedPreferences
import android.preference.PreferenceManager
import dagger.Module
import dagger.Provides
import nl.app.com.jtest.data.repository.ResourceRepository
import javax.inject.Singleton

@Module
class UtilModule {

    @Provides
    @Singleton
    fun provideSharedPreferences(app: Application) : SharedPreferences {
        return PreferenceManager.getDefaultSharedPreferences(app.applicationContext)
    }

    @Provides
    @Singleton
    fun provideResourceProvider(app: Application) = ResourceRepository(app)


}