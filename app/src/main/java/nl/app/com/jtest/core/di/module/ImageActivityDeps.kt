package nl.app.com.jtest.core.di.module

import dagger.Module
import dagger.Provides
import nl.app.com.jtest.presentation.activity.ImageActivity

@Module
class ImageActivityDeps {
    @Provides
    fun provideRouter(a: ImageActivity) = ImageActivity.Router(a)
}