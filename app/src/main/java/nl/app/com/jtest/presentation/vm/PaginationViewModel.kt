package nl.app.com.jtest.presentation.vm

interface PaginationViewModel {

    fun loadFirst()
    fun loadNext()
    fun reload()

    fun getPageSize() : Int
}