package nl.app.com.jtest.presentation.adapterdelegate

import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView

//generally, I prefer using adapter delegates library.
//Every list I had at production had to have several view types
//for the simplicity of this example (just one view type) didn't use it here
abstract class AbsDiffAdapter<T, VH: DelegateViewHolder<T, Any>>(
        diffCallback: DiffUtil.ItemCallback<T>
) : RecyclerView.Adapter<VH>() {

    @Suppress("LeakingThis")
    private val differ = AsyncListDiffer<T>(this, diffCallback)

    fun getItems() : List<T> = differ.currentList
    fun setItems(items: List<T>) = differ.submitList(items)

    override fun getItemCount(): Int = differ.currentList.size

    override fun onBindViewHolder(holder: VH, position: Int) {
        holder.bind(getItems()[position], null)
    }

    override fun onBindViewHolder(holder: VH, position: Int, payloads: MutableList<Any>) {
        holder.bind(getItems()[position], payloads)
    }
}