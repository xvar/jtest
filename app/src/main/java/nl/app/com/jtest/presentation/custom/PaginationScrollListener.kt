package nl.app.com.jtest.presentation.custom

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class PaginationScrollListener(
        private val loadAction: () -> Unit,
        private val layoutManager: LinearLayoutManager,
        private val threshold: Int
) : RecyclerView.OnScrollListener() {

    private var lastTotalItemNumberOnLoad : Int? = null

    fun clear() {
        lastTotalItemNumberOnLoad = null
    }

    private fun onLoadMore() {
        lastTotalItemNumberOnLoad = layoutManager.itemCount
        loadAction()
    }

    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        val totalItemCount = layoutManager.itemCount
        val firstVisibleItem = layoutManager.findFirstVisibleItemPosition()
        if (!(totalItemCount > 0 && firstVisibleItem > 0)) {
            return
        }

        val visibleItemCount = recyclerView.childCount
        if ((totalItemCount - visibleItemCount <= firstVisibleItem + threshold) &&
                (lastTotalItemNumberOnLoad != totalItemCount)
                && dy > 0
        ) {
            onLoadMore()
        }
    }
}