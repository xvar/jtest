package nl.app.com.jtest.data.api

import io.reactivex.Single
import nl.app.com.jtest.BuildConfig
import nl.app.com.jtest.data.model.ImageDTO
import nl.app.com.jtest.data.model.TrendingDTO
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

private const val API_KEY_PARAM = "?api_key=${BuildConfig.API_KEY}"
interface ApiService {

    @GET("/v1/gifs/trending$API_KEY_PARAM")
    fun getTrending(@Query("limit") limit: Int, @Query("offset") offset: Int) : Single<TrendingDTO>

    @GET("/v1/gifs/{id}$API_KEY_PARAM")
    fun getImage(@Path("id") id : String) : Single<ImageDTO>
}