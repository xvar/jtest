package nl.app.com.jtest.core.di.module

import dagger.Module
import dagger.android.ContributesAndroidInjector
import nl.app.com.jtest.presentation.activity.ImageActivity

@Suppress("unused")
@Module
abstract class ActivityBuildersModule {
    @ContributesAndroidInjector(modules = [ImageBindModule::class, ImageActivityDeps::class])
    abstract fun contributeImageListActivity(): ImageActivity
}