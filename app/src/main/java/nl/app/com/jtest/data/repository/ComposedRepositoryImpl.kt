package nl.app.com.jtest.data.repository

import io.reactivex.Flowable
import io.reactivex.Maybe
import io.reactivex.Single
import io.reactivex.processors.BehaviorProcessor
import nl.app.com.jtest.data.api.ApiService
import nl.app.com.jtest.data.model.ImageModel
import nl.app.com.jtest.data.model.PaginationParams
import nl.app.com.jtest.data.model.throwable.NetworkException
import java.util.concurrent.ConcurrentHashMap
import javax.inject.Inject
import javax.inject.Singleton

private const val HTTP_STATUS_OK = 200

//Rx-composed implementation concept
@Singleton
class ComposedImageRepositoryImpl @Inject constructor(
        private val network: ImageNetworkRepository,
        private val cache: ImageCacheRepository
) : ImageRepository {

    override fun getAll(): Flowable<List<ImageModel>> = cache.getAll()

    override fun getTrending(params: PaginationParams): Single<List<ImageModel>> {
        return cache.getTrending(params)
                .concatWith(
                        network.getTrending(params)
                                .doOnSuccess { cache.update(params, it) }
                )
                .filter { it.isNotEmpty() }
                .doOnNext { cache.sendUpdate() }
                .firstOrError()
    }

    override fun getImage(id: String): Maybe<ImageModel> {
        return cache.getImage(id)
                .concatWith(
                        network.getImage(id)
                )
                .firstElement()
    }

    override fun clear() {
        cache.clear()
    }

}

class ImageNetworkRepository @Inject constructor(
        private val apiService: ApiService
) : ImageRepository {

    //for this task - won't use any network cache
    override fun getAll(): Flowable<List<ImageModel>> = Flowable.empty()

    override fun getTrending(params : PaginationParams) : Single<List<ImageModel>> {
        return apiService.getTrending(params.limit, params.offset)
                .map {
                    if (it.meta.status != HTTP_STATUS_OK) {
                        throw NetworkException()
                    }
                    it.data.take(params.limit)
                }
    }


    override fun getImage(id: String): Maybe<ImageModel> {
        return apiService.getImage(id)
                .map {
                    if (it.meta.status != HTTP_STATUS_OK) {
                        throw NetworkException()
                    }
                    it.data
                }
                .toMaybe()
    }

    override fun clear() {}
}

class ImageCacheRepository @Inject constructor() : ImageRepository {

    private val processor = BehaviorProcessor.create<List<ImageModel>>()
    override fun getAll(): Flowable<List<ImageModel>> = processor

    override fun getTrending(params: PaginationParams): Single<List<ImageModel>> {
        return Single.fromCallable {
            val imList = cache[params]
            imList ?: emptyList()
        }
    }

    override fun getImage(id: String): Maybe<ImageModel> {
        return Maybe.fromCallable {
            val model = cache.values.asSequence().flatten().find {
                it.id == id
            }
            model
        }
    }

    private val cache = ConcurrentHashMap<PaginationParams, List<ImageModel>>()

    fun update(params: PaginationParams, list: List<ImageModel>) {
        cache[params] = list
    }

    fun sendUpdate() {
        val list = ArrayList<ImageModel>()
        cache.keys.asSequence().sortedBy { it.offset }.forEach {
            val elements = cache[it]
            if (elements != null) {
                list.addAll(elements)
            }
        }
        processor.onNext(list.distinctBy { it.id })
    }

    override fun clear() = cache.clear()

}