package nl.app.com.jtest.core.di.module

import dagger.Module

@Module(includes = [
    UtilModule::class, ActivityBuildersModule::class, FragmentBuildersModule::class, NetworkModule::class
])
open class AppModule