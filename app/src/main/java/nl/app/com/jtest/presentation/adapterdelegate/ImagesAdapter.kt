package nl.app.com.jtest.presentation.adapterdelegate

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import io.reactivex.Flowable
import io.reactivex.processors.PublishProcessor
import nl.app.com.jtest.R
import nl.app.com.jtest.domain.model.DomainModel
import nl.app.com.jtest.presentation.custom.ImageLoader
import nl.app.com.jtest.presentation.feature.ImageListUIFactory
import nl.app.com.jtest.presentation.model.UIEvent
import nl.app.com.jtest.presentation.viewholder.ImagesViewHolder

class ImagesAdapter(
        private val inflater: LayoutInflater,
        private val uiFactory: ImageListUIFactory,
        private val imageLoader: ImageLoader
) : AbsDiffAdapter<DomainModel.ImageListModel, ImagesViewHolder>(ImageDiff()) {

    private val processor = PublishProcessor.create<UIEvent>()
    fun getEvents(): Flowable<UIEvent> {
        return processor
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImagesViewHolder {
        return ImagesViewHolder(inflater.inflate(R.layout.item_image, parent, false),
                uiFactory, processor, imageLoader)
    }

    override fun getItemId(position: Int): Long {
        return try {
            getItems()[position].stableId
        } catch (ignored: IndexOutOfBoundsException) {
            super.getItemId(position)
        }
    }

    class ImageDiff : DiffUtil.ItemCallback<DomainModel.ImageListModel>() {
        override fun areItemsTheSame(oldItem: DomainModel.ImageListModel, newItem: DomainModel.ImageListModel): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: DomainModel.ImageListModel, newItem: DomainModel.ImageListModel): Boolean {
            return oldItem == newItem
        }
    }
}