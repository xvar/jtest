package nl.app.com.jtest.presentation.custom

import android.widget.ImageView
import androidx.annotation.DrawableRes
import com.facebook.drawee.backends.pipeline.Fresco
import com.facebook.drawee.view.SimpleDraweeView
import nl.app.com.jtest.R
import javax.inject.Inject

interface ImageLoader {
    fun load(url: String, to: ImageView, @DrawableRes placeholder: Int? = null)
}

class FrescoImageLoader @Inject constructor() : ImageLoader {

    override fun load(url: String, to: ImageView, @DrawableRes placeholder: Int?) {
        if(to !is SimpleDraweeView) {
            throw IllegalArgumentException("can't use FrescoImageLoader with only ImageView")
        }
        if (placeholder != null) {
            load(url, to, placeholder)
        } else {
            load(url, to)
        }
    }

    private fun load(url: String, to: SimpleDraweeView, @DrawableRes placeholder: Int? = R.drawable.placeholder) {
        val newController = Fresco.newDraweeControllerBuilder()
                .setAutoPlayAnimations(true)
                .setUri(url)
                .setOldController(to.controller)
                .build()

        placeholder?.let {
            to.hierarchy.setPlaceholderImage(placeholder)
        }

        to.controller = newController
    }
}