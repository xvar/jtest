package nl.app.com.jtest.presentation.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.ViewCompat
import kotlinx.android.synthetic.main.fragment_image_detail.*
import nl.app.com.jtest.R
import nl.app.com.jtest.core.Executors
import nl.app.com.jtest.core.di.ViewModelFactory
import nl.app.com.jtest.core.rx.SchedulerTransformer
import nl.app.com.jtest.domain.interactor.State
import nl.app.com.jtest.domain.model.DomainModel
import nl.app.com.jtest.presentation.custom.ImageLoader
import nl.app.com.jtest.presentation.ktx.getDefaultErrorId
import nl.app.com.jtest.presentation.model.IMAGE_DETAIL_TRANSITION
import nl.app.com.jtest.presentation.model.UIEvent
import nl.app.com.jtest.presentation.vm.ImageDetailViewModel
import javax.inject.Inject

class ImageDetailFragment : BaseFragment() {

    @Inject
    lateinit var vmFactory: ViewModelFactory<ImageDetailViewModel>
    private lateinit var vm: ImageDetailViewModel

    @Inject
    lateinit var executors: Executors
    private lateinit var transformer: SchedulerTransformer.Flowable<State<DomainModel.ImageDetailModel>>

    @Inject
    lateinit var imageLoader: ImageLoader

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_image_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        vm = vmFactory.getForActivity()
        transformer = SchedulerTransformer.Flowable(executors)

        val typeSafeArgs = ImageDetailFragmentArgs.fromBundle(arguments)
        addDisposable("ui",
                vm.observeUI(typeSafeArgs.gifId)
                        .compose(transformer)
                        .subscribe({ bindState(it) } , { showError(it) }
                )
        )
    }

    private fun showError(e: Throwable) {
        Toast.makeText(context, e.getDefaultErrorId(), Toast.LENGTH_LONG).show()
    }

    private fun bindState(state: State<DomainModel.ImageDetailModel>) {
        when(state) {
            is State.Data -> bindImage(state.data)
            is State.Error -> showError(state.e)
            else -> {}
        }
    }

    private fun bindImage(model: DomainModel.ImageDetailModel) {
        model.user?.username?.let {
            name.text = getString(R.string.template_name, it)
        }
        model.user?.displayName?.let {
            nickname.text = getString(R.string.template_nickname, it)
        }
        model.user?.twitter?.let {
            twitter.text = getString(R.string.template_twitter, it)
        }
        model.user?.profileUrl?.let { url ->
            profile.text = url
            profile.setOnClickListener { vm.accept(UIEvent.ProfileClick(url)) }
        }
        imageLoader.load(model.url, image)
        ViewCompat.setTransitionName(image, IMAGE_DETAIL_TRANSITION)

    }

}