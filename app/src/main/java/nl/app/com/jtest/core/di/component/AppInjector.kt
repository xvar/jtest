package nl.app.com.jtest.core.di.component

import android.app.Activity
import android.app.Application
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import com.facebook.cache.disk.DiskCacheConfig
import com.facebook.common.util.ByteConstants
import com.facebook.drawee.backends.pipeline.Fresco
import com.facebook.imagepipeline.core.ImagePipelineConfig
import dagger.android.AndroidInjection
import dagger.android.support.AndroidSupportInjection
import dagger.android.support.HasSupportFragmentInjector
import io.reactivex.plugins.RxJavaPlugins
import nl.app.com.jtest.BuildConfig
import nl.app.com.jtest.core.di.Injectable
import nl.app.com.jtest.presentation.App

object AppInjector {
    fun init(app: App) {
        initializeDependencies(app)

        DaggerAppComponent.builder()
                .application(app)
                .build()
                .inject(app)
        app
                .registerActivityLifecycleCallbacks(object : Application.ActivityLifecycleCallbacks {
                    override fun onActivityCreated(activity: Activity, savedInstanceState: Bundle?) {
                        handleActivity(activity)
                    }

                    override fun onActivityStarted(activity: Activity) {

                    }

                    override fun onActivityResumed(activity: Activity) {

                    }

                    override fun onActivityPaused(activity: Activity) {

                    }

                    override fun onActivityStopped(activity: Activity) {

                    }

                    override fun onActivitySaveInstanceState(activity: Activity, outState: Bundle?) {

                    }

                    override fun onActivityDestroyed(activity: Activity) {

                    }
                })
    }

    private fun initializeDependencies(app: App) {
        val previewsDiskConfig = DiskCacheConfig.newBuilder(app)
                .setMaxCacheSize(250L * ByteConstants.MB).build()
        val qualityDiskConfig = DiskCacheConfig.newBuilder(app)
                .setMaxCacheSize(250L * ByteConstants.MB).build()
        val config = ImagePipelineConfig.newBuilder(app)
                .setSmallImageDiskCacheConfig(previewsDiskConfig)
                .setMainDiskCacheConfig(qualityDiskConfig)
                .build()
        Fresco.initialize(app, config)

        RxJavaPlugins.setErrorHandler { throwable ->
            if (BuildConfig.DEBUG) {
                Log.e("rx_error", "throwable", throwable)
            }
        }
    }

    private fun handleActivity(activity: Activity) {
        if (activity is HasSupportFragmentInjector) {
            AndroidInjection.inject(activity)
        }
        if (activity is FragmentActivity) {
            activity.supportFragmentManager
                    .registerFragmentLifecycleCallbacks(
                            object : FragmentManager.FragmentLifecycleCallbacks() {
                                override fun onFragmentCreated(
                                        fm: FragmentManager,
                                        f: Fragment,
                                        savedInstanceState: Bundle?
                                ) {
                                    if (f is Injectable) {
                                        AndroidSupportInjection.inject(f)
                                    }
                                }
                            }, true
                    )
        }
    }
}