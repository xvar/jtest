package nl.app.com.jtest.data.model

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty

@JsonIgnoreProperties(ignoreUnknown = true)
data class UserModel(
        @JsonProperty("avatar_url") val avatarUrl : String,
        @JsonProperty("profile_url") val profileUrl : String,
        @JsonProperty("username") val username : String,
        @JsonProperty("display_name") val displayName : String,
        @JsonProperty("twitter") val twitter : String?
)