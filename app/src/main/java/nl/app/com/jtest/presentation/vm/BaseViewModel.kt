package nl.app.com.jtest.presentation.vm

import androidx.lifecycle.ViewModel
import io.reactivex.Flowable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.processors.PublishProcessor
import nl.app.com.jtest.core.DisposableMap
import nl.app.com.jtest.presentation.model.RouteEvent

open class BaseViewModel : ViewModel() {
    private val compositeDisposable = CompositeDisposable()
    private val compositeDisposableMap = DisposableMap()
    protected val routeProcessor = PublishProcessor.create<RouteEvent>()

    fun routeEvents() : Flowable<RouteEvent> {
        return routeProcessor
    }

    fun addDisposable(d: Disposable) = compositeDisposable.add(d)
    fun addDisposable(key: String, d: Disposable) = compositeDisposableMap.add(key, d)

    fun clearAll() {
        compositeDisposable.clear()
        compositeDisposableMap.clear()
    }

    override fun onCleared() {
        super.onCleared()
        clearAll()
    }
}