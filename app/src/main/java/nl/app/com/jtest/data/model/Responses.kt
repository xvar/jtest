package nl.app.com.jtest.data.model

import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.databind.annotation.JsonDeserialize

data class TrendingDTO(
        @JsonDeserialize(using = ImageDeserializer::class)
        @JsonProperty("data") val data : List<ImageModel>,
        @JsonProperty("pagination") val pagination : Pagination,
        @JsonProperty("meta") val meta : Meta
)

data class ImageDTO(
        @JsonProperty("data") val data : ImageModel,
        @JsonProperty("meta") val meta : Meta
)