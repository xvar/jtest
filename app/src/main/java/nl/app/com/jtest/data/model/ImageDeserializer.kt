package nl.app.com.jtest.data.model

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonDeserializer
import com.fasterxml.jackson.databind.ObjectMapper

class ImageDeserializer : JsonDeserializer<List<ImageModel>>() {
    override fun deserialize(p: JsonParser?, ctxt: DeserializationContext?): List<ImageModel> {
        val mapper = ObjectMapper()
        val typeFactory = mapper.typeFactory
        val type = typeFactory.constructCollectionType(List::class.java, ImageModel::class.java)
        return mapper.readValue(p, type)
    }
}