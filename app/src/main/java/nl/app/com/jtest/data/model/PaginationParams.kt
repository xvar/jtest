package nl.app.com.jtest.data.model

data class PaginationParams(val offset: Int, val limit: Int)