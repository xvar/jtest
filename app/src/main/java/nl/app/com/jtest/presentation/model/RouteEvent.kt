package nl.app.com.jtest.presentation.model

import android.view.View

const val ROUTE_IMAGE_DETAIL = -10_000
const val ROUTE_USER_PROFILE = -10_001

sealed class RouteEvent(val id: Int) {
    data class ImageDetail(val imageId: String, val shared: View) : RouteEvent(ROUTE_IMAGE_DETAIL)
    data class UserProfile(val url: String): RouteEvent(ROUTE_USER_PROFILE)
}