package nl.app.com.jtest.data.model

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty

@JsonIgnoreProperties(ignoreUnknown = true)
data class ImageModel(
        @JsonProperty("images") val images : Images,
        @JsonProperty("id") val id : String,
        @JsonProperty("user") val user : UserModel?
)