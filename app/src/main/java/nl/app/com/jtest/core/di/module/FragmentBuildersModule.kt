package nl.app.com.jtest.core.di.module

import dagger.Module
import dagger.android.ContributesAndroidInjector
import nl.app.com.jtest.presentation.fragment.ImageDetailFragment
import nl.app.com.jtest.presentation.fragment.ImageListFragment

@Suppress("unused")
@Module
abstract class FragmentBuildersModule {
    @ContributesAndroidInjector(modules = [ImageBindModule::class])
    abstract fun contributeImageListFragment(): ImageListFragment

    @ContributesAndroidInjector(modules = [ImageBindModule::class])
    abstract fun contributeImageDetailFragment(): ImageDetailFragment
}