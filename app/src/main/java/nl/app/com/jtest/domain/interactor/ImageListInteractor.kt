package nl.app.com.jtest.domain.interactor

import io.reactivex.Flowable
import io.reactivex.processors.PublishProcessor
import nl.app.com.jtest.core.DisposableMap
import nl.app.com.jtest.core.Executors
import nl.app.com.jtest.core.rx.LoadingTransformer
import nl.app.com.jtest.core.rx.SchedulerTransformer
import nl.app.com.jtest.data.model.ImageModel
import nl.app.com.jtest.data.repository.ImageRepository
import nl.app.com.jtest.domain.model.DomainModel
import nl.app.com.jtest.domain.model.ImageListModelMapper
import nl.app.com.jtest.domain.pagination.BasePaginationDelegate
import java.util.concurrent.atomic.AtomicBoolean
import javax.inject.Inject

typealias ImageListPaginationInteractor = PaginationInteractor<@JvmSuppressWildcards List<@JvmSuppressWildcards DomainModel.ImageListModel>>

class ImageListInteractor @Inject constructor(
        private val paginationDelegate: BasePaginationDelegate,
        private val mapper: ImageListModelMapper,
        private val repository: ImageRepository,
        private val executors: Executors
) : ImageListPaginationInteractor {

    private val dMap = DisposableMap()
    private val stopPagination = AtomicBoolean(false)
    private val isLoading = AtomicBoolean(false)
    private val loadingTransformer = LoadingTransformer.Single<List<ImageModel>>(isLoading)
    private val schedulerTransformer = SchedulerTransformer.Single<List<ImageModel>>(executors)

    private val all = PublishProcessor.create<State<List<DomainModel.ImageListModel>>>()

    init {
        repository.getAll()
                .map(mapper)
                .map { State.Data(it) }
                .doOnNext { deliverLoading(false) }
                .subscribe(all)
    }

    override fun loadFirst() {
        stopPagination.set(false)
        clear()
        deliverLoading(true)
        dMap.add("first_page", repository.getTrending(paginationDelegate.firstParams())
                .compose(loadingTransformer)
                .compose(schedulerTransformer)
                .subscribe { _, e -> e?.let { deliverError(it) } }
        )
    }

    override fun loadNext() {
        if (isLoading.get() || stopPagination.get())
            return

        deliverLoading(true)
        dMap.add("next_page", repository.getTrending(paginationDelegate.nextParams())
                .compose(loadingTransformer)
                .compose(schedulerTransformer)
                .subscribe { _, e -> e?.let { deliverError(it) } }
        )
    }

    override fun reload() {
        deliverLoading(true)
        dMap.add("reload", repository.getTrending(paginationDelegate.reloadParams())
                .compose(loadingTransformer)
                .compose(schedulerTransformer)
                .subscribe { _, e -> e?.let { deliverError(it) } }
        )
    }

    private fun deliverLoading(isLoading: Boolean) {
        all.onNext(State.Loading(isLoading))
    }

    private fun deliverError(e: Throwable) {
        paginationDelegate.setError(e)
        deliverLoading(false)
        all.onNext(State.Error(e))
    }

    override fun clear() {
        dMap.clear()
        repository.clear()
        paginationDelegate.reset()
    }

    override fun getPageSize(): Int = paginationDelegate.pageSize

    override fun stopPagination(): Boolean = stopPagination.get()

    override fun observe(): Flowable<State<List<DomainModel.ImageListModel>>> = all
}