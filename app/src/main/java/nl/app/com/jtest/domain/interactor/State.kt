package nl.app.com.jtest.domain.interactor

//could be modeled with just one class
//it's preference matter to me
sealed class State<T> {
    data class Loading<T>(val isLoading: Boolean): State<T>()
    data class Error<T>(val e: Throwable): State<T>()
    data class Data<T>(val data: T) : State<T>()
}