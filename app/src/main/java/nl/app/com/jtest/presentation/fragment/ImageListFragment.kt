package nl.app.com.jtest.presentation.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import nl.app.com.jtest.core.Executors
import nl.app.com.jtest.core.di.Injectable
import nl.app.com.jtest.core.di.ViewModelFactory
import nl.app.com.jtest.databinding.FragmentImageListBinding
import nl.app.com.jtest.domain.interactor.State
import nl.app.com.jtest.domain.pagination.PaginationConfig
import nl.app.com.jtest.presentation.adapterdelegate.ImagesAdapter
import nl.app.com.jtest.presentation.custom.ImageLoader
import nl.app.com.jtest.presentation.custom.PaginationScrollListener
import nl.app.com.jtest.presentation.feature.ImageListUIFactory
import nl.app.com.jtest.presentation.ktx.getDefaultErrorId
import nl.app.com.jtest.presentation.model.UIEvent
import nl.app.com.jtest.presentation.vm.ImageListViewModel
import javax.inject.Inject

class ImageListFragment : BaseFragment(), Injectable {

    private lateinit var binding: FragmentImageListBinding
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentImageListBinding.inflate(inflater, container, false)
        return binding.root
    }

    @Inject
    lateinit var vmFactory: ViewModelFactory<ImageListViewModel>
    @Inject
    lateinit var executors: Executors
    @Inject
    lateinit var uiFactory: ImageListUIFactory
    @Inject
    lateinit var paginationConfig: PaginationConfig
    @Inject
    lateinit var imageLoader: ImageLoader

    private lateinit var vm : ImageListViewModel
    private lateinit var adapter: ImagesAdapter
    private var reload = false

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        vm = vmFactory.getForActivity()
        initAdapter()

        val lm = uiFactory.getLayoutManager(activity!!)
        val scrollListener = PaginationScrollListener({ vm.loadNext() }, lm, paginationConfig.getThreshold())

        binding.rv.layoutManager = lm
        adapter.setHasStableIds(true)
        binding.rv.adapter = adapter
        binding.rv.addItemDecoration(uiFactory.getItemDecoration())
        binding.rv.addOnScrollListener(scrollListener)
        binding.reload.setOnClickListener { vm.accept(UIEvent.ReloadClick()) }
        binding.toggle.setOnClickListener { vm.accept(UIEvent.ToggleClick()) }
        binding.swipe.setOnRefreshListener { vm.loadFirst() }

        addDisposable("vm", adapter.getEvents().subscribe(vm))

        addDisposable("ui_list", vm.uiState()
                .subscribe {
                    binding.reload.visibility = View.GONE
                    when (it) {
                        is State.Loading -> { binding.swipe.isRefreshing = it.isLoading }
                        is State.Error -> {
                            Toast.makeText(activity, it.e.getDefaultErrorId(), Toast.LENGTH_LONG)
                                 .show()
                            binding.reload.visibility = View.VISIBLE
                        }
                        is State.Data -> {
                            adapter.setItems(it.data)
                            scrollListener.clear()
                        }
                    }
                }
        )

        addDisposable("toggle", vm.toggleState().subscribe {
            binding.toggle.setImageResource(it)
        })

        if (reload) {
            vm.loadFirst()
            reload = false
        }
    }

    //Well, without LiveData or custom NavController, fragment is being replaced every time via
    //navigation framework. We want to keep user's scroll position, so boolean flag is used.
    //Not very beautiful, though works.
    private fun initAdapter() {
        val newAdapter = ImagesAdapter(LayoutInflater.from(activity), uiFactory, imageLoader)
        try {
            newAdapter.setItems(adapter.getItems())
        } catch (e: UninitializedPropertyAccessException) {
            reload = true
        }
        adapter = newAdapter
    }
}