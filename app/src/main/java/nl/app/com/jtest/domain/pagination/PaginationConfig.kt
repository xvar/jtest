package nl.app.com.jtest.domain.pagination

import javax.inject.Inject

class PaginationConfig @Inject constructor() {
    private val defaultPageSize = 30
    fun getPageSize() = defaultPageSize
    fun getThreshold() = defaultPageSize / 2
}