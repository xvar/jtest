package nl.app.com.jtest.core.rx

import io.reactivex.*
import nl.app.com.jtest.core.Executors
import org.reactivestreams.Publisher

sealed class SchedulerTransformer {
    class Single<U>(private val executors: Executors): SingleTransformer<U, U> {
        override fun apply(upstream: io.reactivex.Single<U>): SingleSource<U> {
            return upstream
                    .subscribeOn(executors.io())
                    .observeOn(executors.main())
        }
    }

    class Flowable<U>(private val executors: Executors): FlowableTransformer<U, U> {
        override fun apply(upstream: io.reactivex.Flowable<U>): Publisher<U> {
            return upstream
                    .subscribeOn(executors.io())
                    .observeOn(executors.main())
        }
    }

    class Observable<U>(private val executors: Executors): ObservableTransformer<U, U> {
        override fun apply(upstream: io.reactivex.Observable<U>): ObservableSource<U> {
            return upstream
                    .subscribeOn(executors.io())
                    .observeOn(executors.main())
        }
    }

    class Completable(private val executors: Executors): CompletableTransformer {
        override fun apply(upstream: io.reactivex.Completable): CompletableSource {
            return upstream
                    .subscribeOn(executors.io())
                    .observeOn(executors.main())
        }
    }

    class Maybe<U>(private val executors: Executors): MaybeTransformer<U, U> {
        override fun apply(upstream: io.reactivex.Maybe<U>): MaybeSource<U> {
            return upstream
                    .subscribeOn(executors.io())
                    .observeOn(executors.main())
        }
    }
}