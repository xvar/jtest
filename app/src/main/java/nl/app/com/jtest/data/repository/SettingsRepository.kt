package nl.app.com.jtest.data.repository

import android.content.SharedPreferences
import javax.inject.Inject

class SettingsRepository @Inject constructor(private val shp: SharedPreferences) {
    private val imageSettingsKey = "SettingsRepository_imageSettings"

    fun isStaticImage() = shp.getBoolean(imageSettingsKey, false)
    fun toggleImageType() : Boolean {
        val isStatic = isStaticImage()
        val res = !isStatic
        shp.edit()
                .putBoolean(imageSettingsKey, res)
                .apply()
        return res
    }
}