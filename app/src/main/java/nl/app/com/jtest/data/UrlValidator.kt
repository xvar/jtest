package nl.app.com.jtest.data

import android.content.Intent
import android.net.Uri
import nl.app.com.jtest.data.repository.ResourceRepository
import javax.inject.Inject

class UrlValidator @Inject constructor(private val r: ResourceRepository) {

    fun canOpenUrl(url: String) : Boolean {
        return try {
            val intent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
            intent.resolveActivity(r.packageManager) != null
        } catch (t: Throwable) {
            false
        }
    }

}