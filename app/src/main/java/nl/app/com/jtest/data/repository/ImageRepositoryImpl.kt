package nl.app.com.jtest.data.repository

import io.reactivex.Flowable
import io.reactivex.Maybe
import io.reactivex.Single
import io.reactivex.processors.BehaviorProcessor
import nl.app.com.jtest.data.api.ApiService
import nl.app.com.jtest.data.model.ImageModel
import nl.app.com.jtest.data.model.PaginationParams
import nl.app.com.jtest.data.model.throwable.NetworkException
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.locks.ReentrantReadWriteLock
import javax.inject.Inject
import javax.inject.Singleton
import kotlin.concurrent.read
import kotlin.concurrent.write

private const val HTTP_STATUS_OK = 200

//I'll use this implementation, since it has less overhead
//But, if we have several data sources (more than two), the other implementation will suit better,
//since it's easier to expand. See ComposedRepositoryImpl
@Singleton
class ImageRepositoryImpl @Inject constructor(
        private val apiService: ApiService
) : ImageRepository {
    private val cache = ConcurrentHashMap<PaginationParams, List<ImageModel>>()
    private val processor = BehaviorProcessor.create<List<ImageModel>>()
    private val lock = ReentrantReadWriteLock()

    override fun getTrending(params: PaginationParams): Single<List<ImageModel>> {
        lock.write {
            return if (cache.containsKey(params)) {
                 Single.fromCallable {
                     notifySubs()
                     cache[params]
                 }
            } else {
                 apiService.getTrending(params.limit, params.offset)
                        .map {
                            if (it.meta.status != HTTP_STATUS_OK) {
                                throw NetworkException()
                            }
                            it.data.take(params.limit)
                        }
                        .doOnSuccess {
                            cache[params] = it
                            notifySubs()
                        }
            }
        }
    }

    private fun notifySubs() {
        lock.write {
            val idSet = HashSet<String>()
            val list = ArrayList<ImageModel>()
            cache.keys.asSequence().sortedBy { it.offset }.forEach { params ->
                cache[params]?.forEach {
                    if (idSet.add(it.id))
                        list.add(it)
                }
            }
            processor.onNext(list)
        }
    }

    override fun getImage(id: String): Maybe<ImageModel> {
        lock.read {
            val value = cache.values.asSequence().flatten().find { it.id == id }
            return if (value != null) {
                Maybe.just(value)
            } else {
                apiService.getImage(id).map {
                    if (it.meta.status != HTTP_STATUS_OK) {
                        throw NetworkException()
                    }
                    it.data
                }.toMaybe()
            }
        }
    }

    override fun getAll(): Flowable<List<ImageModel>> = processor

    override fun clear() {
        lock.write {
            cache.clear()
        }
    }
}