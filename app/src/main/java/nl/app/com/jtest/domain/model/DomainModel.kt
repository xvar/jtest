package nl.app.com.jtest.domain.model

import nl.app.com.jtest.data.model.UserModel

sealed class DomainModel {
    abstract val stableId : Long
    data class ImageListModel(val url: String, val id: String): DomainModel() {
        override val stableId: Long
            get() = id.hashCode().toLong()
    }

    data class ImageDetailModel(val url: String, val user: UserModel?)
}