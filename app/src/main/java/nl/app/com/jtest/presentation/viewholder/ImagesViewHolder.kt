package nl.app.com.jtest.presentation.viewholder

import android.view.View
import com.facebook.drawee.view.SimpleDraweeView
import nl.app.com.jtest.R
import nl.app.com.jtest.domain.model.DomainModel
import nl.app.com.jtest.presentation.adapterdelegate.DelegateViewHolder
import nl.app.com.jtest.presentation.custom.ImageLoader
import nl.app.com.jtest.presentation.feature.ImageListUIFactory
import nl.app.com.jtest.presentation.model.UIEvent
import org.reactivestreams.Processor

//won't use payloads in this example (at least, for now)
class ImagesViewHolder(
        private val containerView: View,
        private val uiFactory: ImageListUIFactory,
        private val processor: Processor<UIEvent, UIEvent>,
        private val imageLoader: ImageLoader
): DelegateViewHolder<DomainModel.ImageListModel, Any>(containerView) {
    private val simpleDrawee = containerView.findViewById<SimpleDraweeView>(R.id.list_image)
    init {
        val sizePair = uiFactory.getImageSizePx()
        val lp = simpleDrawee.layoutParams
        lp.width = sizePair.first
        lp.height = sizePair.second
        simpleDrawee.layoutParams = lp
    }

    override fun bind(item: DomainModel.ImageListModel) {
        imageLoader.load(item.url, simpleDrawee)
        simpleDrawee.setOnClickListener { processor.onNext(UIEvent.ImageClick(item.id, simpleDrawee)) }
    }

}
