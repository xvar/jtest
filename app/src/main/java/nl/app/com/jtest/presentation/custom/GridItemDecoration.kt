package nl.app.com.jtest.presentation.custom

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView

class GridItemDecoration(
        private val offset: Int
) : RecyclerView.ItemDecoration() {

    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView,
                                state: RecyclerView.State) {

        with(outRect) {
            val position = parent.getChildAdapterPosition(view)
            val lm = parent.layoutManager

            if (position == RecyclerView.NO_POSITION) return@with
            if (lm !is GridLayoutManager) {
                throw IllegalStateException("GridLayoutManager is required")
            }

            top = offset

            val spanCount = lm.spanCount
            val spanSizeLookup = lm.spanSizeLookup
            val spanSize = spanSizeLookup.getSpanSize(position)
            val spanIndex = spanSizeLookup.getSpanIndex(position, spanCount)
            if (spanSize == spanCount) {
                left = offset
                right = offset
                return@with
            }

            when (spanIndex) {
                0 -> left = offset
                spanCount - 1 -> right = offset
                else -> {
                    left = offset / 2
                    right = offset / 2
                }
            }
        }
    }
}
