package nl.app.com.jtest.domain.pagination

import nl.app.com.jtest.data.model.PaginationParams
import java.util.concurrent.atomic.AtomicInteger
import java.util.concurrent.atomic.AtomicReference
import javax.inject.Inject

class BasePaginationDelegate @Inject constructor (private val config: PaginationConfig) : PaginationDelegate {
    private val page = AtomicInteger(0)
    private val error = AtomicReference<Throwable>(null)
    override val pageSize: Int
        get() = config.getPageSize()

    override fun setError(e: Throwable?) {
        error.set(e)
    }

    override fun firstParams() : PaginationParams {
        page.set(0)
        error.set(null)
        return PaginationParams(0, pageSize)

    }

    override fun nextParams() : PaginationParams {
        val pageNumber = if (error.get() != null) page.get() else page.incrementAndGet()
        return PaginationParams(pageNumber * pageSize, pageSize)

    }

    override fun reloadParams() : PaginationParams {
        return PaginationParams(page.get() * pageSize, pageSize)
    }

    fun reset() {
        error.set(null)
        page.set(0)
    }
}