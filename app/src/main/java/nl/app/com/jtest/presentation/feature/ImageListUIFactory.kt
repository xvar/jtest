package nl.app.com.jtest.presentation.feature

import android.app.Activity
import androidx.recyclerview.widget.RecyclerView
import nl.app.com.jtest.R
import nl.app.com.jtest.data.repository.ResourceRepository
import nl.app.com.jtest.presentation.custom.GridItemDecoration
import nl.app.com.jtest.presentation.custom.NpaGridLayoutManager
import javax.inject.Inject
import kotlin.math.roundToInt

class ImageListUIFactory @Inject constructor(
        private val rp: ResourceRepository
) {
    private val offsetSizePx = rp.resources.getDimensionPixelOffset(R.dimen.image_list_side_offset)
    private fun getColumnCount() = 3

    fun getLayoutManager(activity: Activity) = NpaGridLayoutManager(activity, getColumnCount())

    fun getItemDecoration() : RecyclerView.ItemDecoration {
        return GridItemDecoration(offsetSizePx)
    }

    /**
     * Return image width and height in pixels
     */
    fun getImageSizePx() : Pair<Int, Int> {
        val columnCount = getColumnCount()
        val offsetCount = columnCount + 1
        val totalOffsetSize = offsetSizePx * offsetCount
        val imageSide = ((getScreenWidthPx().toDouble() - totalOffsetSize) / columnCount).roundToInt()
        return Pair(imageSide, imageSide)
    }

    private fun getScreenWidthPx() = rp.resources.displayMetrics.widthPixels

}