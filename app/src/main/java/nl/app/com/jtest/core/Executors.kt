package nl.app.com.jtest.core

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class Executors @Inject constructor() {
    //one might use own executors and schedulers, based on top of them
    fun main() = AndroidSchedulers.mainThread()
    fun io() = Schedulers.io()
    fun computation() = Schedulers.computation()
}