package nl.app.com.jtest.presentation.activity

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProviders
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import io.reactivex.disposables.Disposable
import nl.app.com.jtest.core.DisposableMap
import nl.app.com.jtest.core.di.ViewModelFactory
import javax.inject.Inject

abstract class BaseActivity : AppCompatActivity(), HasSupportFragmentInjector {

    private val compositeDisposableMap = DisposableMap()
    protected fun addDisposable(key: String, d: Disposable) = compositeDisposableMap.add(key, d)

    @Inject lateinit var sFI: DispatchingAndroidInjector<Fragment>
    override fun supportFragmentInjector(): AndroidInjector<Fragment> = sFI

    inline fun <reified T : ViewModel> ViewModelFactory<T>.get(): T =
            ViewModelProviders.of(this@BaseActivity, this)[T::class.java]


    override fun onDestroy() {
        super.onDestroy()
        compositeDisposableMap.clear()
    }

}