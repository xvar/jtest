package nl.app.com.jtest.presentation.vm

import io.reactivex.Flowable
import io.reactivex.functions.Consumer
import io.reactivex.processors.BehaviorProcessor
import io.reactivex.subjects.BehaviorSubject
import nl.app.com.jtest.R
import nl.app.com.jtest.core.Executors
import nl.app.com.jtest.core.rx.SchedulerTransformer
import nl.app.com.jtest.data.repository.SettingsRepository
import nl.app.com.jtest.domain.interactor.ImageListPaginationInteractor
import nl.app.com.jtest.domain.interactor.State
import nl.app.com.jtest.domain.model.DomainModel
import nl.app.com.jtest.presentation.model.RouteEvent
import nl.app.com.jtest.presentation.model.UIEvent
import javax.inject.Inject

class ImageListViewModel @Inject constructor(
        private val interactor: ImageListPaginationInteractor,
        private val settingsRepository: SettingsRepository,
        private val executors: Executors
): BaseViewModel(), PaginationViewModel, Consumer<UIEvent> {

    private val toggleSubject = BehaviorProcessor.create<Boolean>()
    init {
        toggleSubject.onNext(settingsRepository.isStaticImage())
    }

    override fun loadFirst() = interactor.loadFirst()

    override fun loadNext() = interactor.loadNext()

    override fun reload() = interactor.reload()

    override fun getPageSize(): Int = interactor.getPageSize()

    override fun accept(event: UIEvent) {
        when(event) {
            is UIEvent.ImageClick -> {
                routeProcessor.onNext(RouteEvent.ImageDetail(event.imageId, event.view))
            }
            is UIEvent.ToggleClick -> {
                settingsRepository.toggleImageType()
                toggleSubject.onNext(settingsRepository.isStaticImage())
                loadFirst()
            }
            is UIEvent.ReloadClick -> reload()
        }
    }

    fun uiState() : Flowable<State<List<DomainModel.ImageListModel>>> = interactor.observe()
            .compose(SchedulerTransformer.Flowable(executors))

    fun toggleState(): Flowable<Int> = toggleSubject.map {
        if (it) R.drawable.ic_play_24px else R.drawable.ic_pause_24px
    }
}