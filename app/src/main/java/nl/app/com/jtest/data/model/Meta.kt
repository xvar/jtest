package nl.app.com.jtest.data.model

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty

@JsonIgnoreProperties(ignoreUnknown = true)
data class Meta(
        @JsonProperty("msg") val msg : String,
        @JsonProperty("status") val status : Int
)