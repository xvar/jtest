package nl.app.com.jtest.presentation.fragment

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProviders
import io.reactivex.disposables.Disposable
import nl.app.com.jtest.core.DisposableMap
import nl.app.com.jtest.core.di.Injectable
import nl.app.com.jtest.core.di.ViewModelFactory

abstract class BaseFragment : Fragment(), Injectable {

    private val compositeDisposableMap = DisposableMap()

    protected fun addDisposable(key: String, d: Disposable) = compositeDisposableMap.add(key, d)
    private fun clearDisposables() {
        compositeDisposableMap.clear()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        clearDisposables()
    }

    inline fun <reified T : ViewModel> ViewModelFactory<T>.get(): T =
            ViewModelProviders.of(this@BaseFragment, this)[T::class.java]

    inline fun <reified T : ViewModel> ViewModelFactory<T>.getForActivity(): T =
            ViewModelProviders.of(activity!!, this)[T::class.java]
}