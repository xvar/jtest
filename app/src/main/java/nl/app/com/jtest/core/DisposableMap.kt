package nl.app.com.jtest.core

import io.reactivex.disposables.Disposable

class DisposableMap {

    private val map = HashMap<String, Disposable?>()

    @Synchronized
    fun clear(key: String) {
        map[key]?.dispose()
        map[key] = null
    }

    @Synchronized
    fun add(key : String, d: Disposable) {
        clear(key)
        map[key] = d
    }

    @Synchronized
    fun clear() {
        map.mapKeys { clear(it.key) }
        map.clear()
    }

}