package nl.app.com.jtest.domain.pagination

import nl.app.com.jtest.data.model.PaginationParams

interface PaginationDelegate {
    fun setError(e: Throwable?)
    fun firstParams() : PaginationParams
    fun nextParams() : PaginationParams
    fun reloadParams() : PaginationParams
    val pageSize : Int
}