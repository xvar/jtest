package nl.app.com.jtest.data.model

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty

@JsonIgnoreProperties(ignoreUnknown = true)
data class Images(
        @JsonProperty("fixed_width") val fixedWidth: FixedWidth,
        @JsonProperty("fixed_width_still") val fixedWidthStill: FixedWidthStill
)

@JsonIgnoreProperties(ignoreUnknown = true)
data class FixedWidth(
    @JsonProperty("url") val url: String
)

@JsonIgnoreProperties(ignoreUnknown = true)
data class FixedWidthStill(
        @JsonProperty("url") val url: String
)

