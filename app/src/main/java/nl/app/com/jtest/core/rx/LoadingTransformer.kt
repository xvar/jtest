package nl.app.com.jtest.core.rx

import io.reactivex.*
import org.reactivestreams.Publisher
import java.util.concurrent.atomic.AtomicBoolean

sealed class LoadingTransformer {

    class Single<U>(private val isLoading: AtomicBoolean): SingleTransformer<U, U> {
        override fun apply(upstream: io.reactivex.Single<U>): SingleSource<U> {
            return upstream
                .doOnSubscribe { isLoading.set(true) }
                .doOnEvent { _, _ -> isLoading.set(false) }
        }
    }

    class Flowable<U>(private val isLoading: AtomicBoolean): FlowableTransformer<U, U> {
        override fun apply(upstream: io.reactivex.Flowable<U>): Publisher<U> {
            return upstream
                    .doOnSubscribe { isLoading.set(true) }
                    .doOnEach { _ -> isLoading.set(false) }
        }
    }

    class Observable<U>(private val isLoading: AtomicBoolean): ObservableTransformer<U, U> {
        override fun apply(upstream: io.reactivex.Observable<U>): ObservableSource<U> {
            return upstream
                    .doOnSubscribe { isLoading.set(true) }
                    .doOnEach { _ -> isLoading.set(false) }
        }
    }

    class Completable(private val isLoading: AtomicBoolean): CompletableTransformer {
        override fun apply(upstream: io.reactivex.Completable): CompletableSource {
            return upstream
                    .doOnSubscribe { isLoading.set(true) }
                    .doOnEvent { _ -> isLoading.set(false) }
        }
    }

    class Maybe<U>(private val isLoading: AtomicBoolean): MaybeTransformer<U, U> {
        override fun apply(upstream: io.reactivex.Maybe<U>): MaybeSource<U> {
            return upstream
                    .doOnSubscribe { isLoading.set(true) }
                    .doOnEvent { _, _ -> isLoading.set(false) }
        }
    }

}
