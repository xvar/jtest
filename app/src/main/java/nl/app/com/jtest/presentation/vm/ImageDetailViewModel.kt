package nl.app.com.jtest.presentation.vm

import io.reactivex.Flowable
import io.reactivex.functions.Consumer
import io.reactivex.processors.PublishProcessor
import nl.app.com.jtest.data.UrlValidator
import nl.app.com.jtest.data.model.throwable.UrlNotSupportedException
import nl.app.com.jtest.data.repository.ImageRepository
import nl.app.com.jtest.domain.interactor.State
import nl.app.com.jtest.domain.model.DomainModel
import nl.app.com.jtest.domain.model.ImageDetailMapper
import nl.app.com.jtest.presentation.model.RouteEvent
import nl.app.com.jtest.presentation.model.UIEvent
import javax.inject.Inject

class ImageDetailViewModel @Inject constructor(
        private val repository: ImageRepository,
        private val imageDetailMapper: ImageDetailMapper,
        private val urlValidator: UrlValidator
): BaseViewModel(), Consumer<UIEvent> {

    private val errorProcessor = PublishProcessor.create<State<DomainModel.ImageDetailModel>>()

    override fun accept(event: UIEvent) {
        if (event is UIEvent.ProfileClick) {

            if (!urlValidator.canOpenUrl(event.url)) {
                errorProcessor.onNext(State.Error(UrlNotSupportedException()))
            } else {
                routeProcessor.onNext(RouteEvent.UserProfile(event.url))
            }
        }
    }

    fun observeUI(id: String): Flowable<State<DomainModel.ImageDetailModel>> {
        return repository.getImage(id)
                .map(imageDetailMapper)
                .map { State.Data(it) as State<DomainModel.ImageDetailModel> }
                .mergeWith { errorProcessor }
    }
}